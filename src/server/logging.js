/**
		* OnEd. Create an editor to listen on port 8080.
		*
		* This code is under the MIT license.
		*/
import _ from 'lodash';
import {loggers} from 'winston';

/**
  * The default logging level. Usually 'info'.
	*
	* @type {String}
	* @private
	* @default 'info'
	* @see setDefaultLoggingLevel()
	* @see getDefaultLoggingLevel()
	*/
let defaultLoggingLevel = 'info';

/**
  * Sets the default logging level.
	*
	* @param {String} value What to set the level to.
	*/
export function setDefaultLoggingLevel(value) {
	defaultLoggingLevel = value;
}

/**
	* Gets the default logging level.
	*
	* @returns {String} The current logging level.
	*/
export function getDefaultLoggingLevel() {
	return defaultLoggingLevel;
}

/**
  * Creates a new Winston logger and returns it.
		*
		* @param {String} name The name that the logger will use.
		* @param {Object} transports Optional - transports to use in the Winston logger.
		* @param {String} level Optional - set the level on the logger.
		* @returns {WinstonLogger} The Winston logger.
		* @example
		* const logger = getWinstonLogger("main");
		* logger.info("Hello, World!");
		* @todo Add a new example for transports.
		*/
export default function getWinstonLoggerDefault(name, _transports, _level) {
	let transports;
	let level;
	if (_.isObject(_transports) && !_.isString(_transports)) {
		transports = _transports;
	} else {
		transports = {};
	}
	if (_.isString(_transports)) {
		level = _transports;
	} else if (_.isString(_level)) {
		level = _level;
	} else {
		level = getDefaultLoggingLevel();
	}

	transports = _.merge(transports, {
		console: {
			label: name,
			level,
			colorize: true
		}
	});

	loggers.add(name, transports);
	return loggers.get(name);
}

export function getWinstonLogger(name, transports, level) {
	return getWinstonLoggerDefault(name, transports, level);
}
