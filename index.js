/**
		* OnEd. Create an editor to listen on port 8080.
		*
		* This code is under the MIT license.
		*/
import {safeLoad as yaml} from 'js-yaml';
import {readFile} from '@jokeyrhyme/pify-fs';
import {join as pathJoin} from 'path';
import {merge} from 'lodash';
import parseArgs from 'minimist';
import {getWinstonLogger} from './src/server/logging';

let logger;

readFile(pathJoin(__dirname, '/config.yml'), 'utf-8').then(yaml)
	.then(data =>
		merge(parseArgs(process.argv.slice(2)), data)
	).then(data => {
		logger = getWinstonLogger('main', data.verbose ? 'verbose' : 'info');
		logger.info('Read configuration from config.yml and argv, loading server');
	})
	.catch(err => {
		(logger || console).error(logger ? err.stack : err);
	});
