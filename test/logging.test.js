import {isObject} from 'lodash';
import test from 'tape-extra';
import {Logger, loggers} from 'winston';
import {getWinstonLogger, setDefaultLoggingLevel, getDefaultLoggingLevel} from '../src/server/logging';

test.beforeEach = done => {
	loggers.close();
	done();
};

test('The log generator should return a WinstonLogger', t => {
	t.true(getWinstonLogger('test') instanceof Logger, 'Logger is an instance of a Winston logger');
	t.true(isObject(getWinstonLogger('test')), 'Logger should be an object');
	t.end();
});

test('The log generator should index it in the Loggers table', t => {
	t.equals(getWinstonLogger('test1'), loggers.get('test1'), 'Logger should be indexed in the table');
	t.end();
});

test('The log generator should allow different transports and levels', t => {
	t.equals(getWinstonLogger('test2', 'verbose').transports.console.level, 'verbose');
	t.equals(getWinstonLogger('test1', {}, 'debug').transports.console.level, 'debug');
	t.end();
});

test('The log generator should be nullsafe', t => {
	t.equals(getWinstonLogger('test2', null, 'verbose').transports.console.level, 'verbose');
	t.equals(getWinstonLogger(null, {}, 'error').transports.console.level, 'error');
	t.end();
});

test('The default getWinstonLogger level should be set and appliable', t => {
	t.equals(getDefaultLoggingLevel(), 'info');
	t.equals(getWinstonLogger('test').transports.console.level, 'info');
	setDefaultLoggingLevel('verbose');
	t.equals(getDefaultLoggingLevel(), 'verbose');
	t.equals(getWinstonLogger('test1').transports.console.level, 'verbose');
	t.end();
});
